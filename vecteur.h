#ifndef VECTEUR_H_
#define VECTEUR_H_

#include <stdlib.h>
#include <stdio.h>

typedef struct Vecteur Vecteur;

struct Vecteur {
    int * p;
    long dernier_indice;
    unsigned long capacite;
};

Vecteur nouveau_vecteur(unsigned long t);

void ajouter_entier(Vecteur * v, int a);
void modifier(Vecteur * v, long indice, int valeur);

int acceder_entier(Vecteur * v, long indice);

int * sous_tableau(Vecteur * v, int a, int b);

void liberer_memoire(Vecteur * v);

#endif // VECTEUR_H_
