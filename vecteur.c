#include "vecteur.h"

Vecteur nouveau_vecteur(unsigned long t) {
    Vecteur res;
    res.p = calloc(sizeof(int), t);
    res.dernier_indice = -1;
    res.capacite = t;
    return res;
}

void ajouter_entier(Vecteur * v, int a)
{
    if ( v->dernier_indice == (long) v->capacite - 1)
    {
        int * nouveau_pointeur;
        nouveau_pointeur = realloc(v->p, (v->capacite) * 2);
        if (nouveau_pointeur == NULL)
            printf("Impossible d agrandir le vecteur");

        else
            v->capacite = v->capacite * 2;
    }

    v->p[v->dernier_indice + 1] = a;
    v->dernier_indice++;
}

int acceder_entier(Vecteur * v, long indice)
{
    // Si l indice est negatif
    if (indice < 0)
        return acceder_entier(v, v->capacite - indice);

    return v->p[indice];
}

void modifier(Vecteur * v, long indice, int valeur) {
    v->p[indice] = valeur;
}

int pop(Vecteur * v)
{
    int res = v->p[v->dernier_indice];
    v->dernier_indice--;
    return res;
}

/*
 * Permet d obtenir le tableau present dans l intervalle [a;b[ du vecteur
 */
int * sous_tableau(Vecteur * v, int a, int b)
{
    int taille = b - a;
    int * resultat = malloc(sizeof(int) * taille);

    for (int i = a; i < b; i++)
        resultat[i] = acceder_entier(v, a + i);

    return resultat;
}

void liberer_memoire(Vecteur * v)
{
    int nouvelle_taille = v->dernier_indice + 1;
    free(v->p + nouvelle_taille);
    v->capacite = nouvelle_taille + 1;
}
